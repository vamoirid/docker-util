# Docker Utilities

## How to build a Docker image

```console
docker build -f Dockerfile.<IMAGE_NAME> -t registry.gitlab.com/vamoirid/docker-util/<IMAGE_NAME> .
```
