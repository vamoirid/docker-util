FROM ubuntu:24.04

ENV DEBIAN_FRONTEND noninteractive

RUN dpkg --add-architecture i386 && \
    apt update -y
RUN apt install -y --no-install-recommends \
        bc \
        build-essential \
        bzr \
        ca-certificates \
        cmake \
        cpio \
        cvs \
	dosfstools \
        file \
        g++-multilib \
        git \
        libc6:i386 \
        libncurses5-dev \
        libssl-dev \
        locales \
        mercurial \
        openssh-server \
        openssl \
        python3 \
        python3-flake8 \
        python3-magic \
        python3-nose2 \
        python3-pexpect \
        python3-pytest \
        qemu-system-arm \
        qemu-system-misc \
        qemu-system-x86 \
        rsync \
        shellcheck \
        subversion \
        unzip \
        wget \
        && \
    apt -y autoremove && \
    apt -y clean

RUN rm -rf /var/lib/{apt,dpkg,cache,log}

RUN export LC_ALL=en_US.UTF-8
RUN export LANG=en_US.UTF-8
RUN locale-gen en_US.UTF-8

RUN git clone https://gitlab.com/buildroot.org/buildroot.git --depth=1 --branch=2024.02 /root/buildroot

RUN useradd -m br2_user && \
    chown -R br2_user:br2_user /root/buildroot

USER br2_user
